require.config({

    baseUrl: 'js/lib',

    paths: {
        app: '../app',
        tpl: '../tpl',
        trans: '../app/translate'
    },

    map: {
        '*': {
            'app/models/client': 'app/models/json/client',
            'app/models/teacher': 'app/models/json/teacher',
            'app/models/driving': 'app/models/json/driving',
            'app/models/apikey': 'app/models/json/apikey'
        }
    },

    shim: {
        'backbone': {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});
define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        Router              = require('app/router'),
        LayoutView          = require('app/views/Layout'),
        Translate           = require('trans/translate');
   
   var app = {

        translate: new Translate(),

        initialize: function() {
            app.bindEvents();
        },
        bindEvents: function() {
            document.addEventListener('deviceready', app.onDeviceReady, false);
        },
        onDeviceReady: function() {
            document.addEventListener('backbutton', app.backPressed, false);
            app.receivedEvent();
            if(navigator.camera) {
                app.setUpCamera();
            }
        },
        receivedEvent: function() {
            if(window.localStorage.lang === undefined) {
                window.localStorage.lang = 'ee';
            }

            var layout = new LayoutView().render();
            var router = new Router({layout: layout});

            $("body").on("click", ".backButton", function (event) {
                event.preventDefault();
                window.history.back();
            });
            Backbone.history.start();
            if(typeof window.plugins !== 'undefined') {
                window.plugins.insomnia.keepAwake();

            }
            
        },
        setUpCamera: function() {
            window.cameraOptions = {  quality : 80,
                                      destinationType : navigator.camera.DestinationType.DATA_URL,
                                      sourceType : navigator.camera.PictureSourceType.CAMERA,
                                      allowEdit : true,
                                      encodingType: Camera.EncodingType.JPEG,
                                      targetWidth: 200,
                                      targetHeight: 200,
                                      saveToPhotoAlbum: false,
                                      correctOrientation: false,
                                      cameraDirection: 0
                                    };
            
            window.destinationType = navigator.camera.DestinationType;
        },
        backPressed: function() {
           var currentUrl = window.location.hash;
            if(currentUrl == '#home' || currentUrl == '#login') { 
                navigator.notification.confirm('Olete kindel, et soovite rakendusest väljuda?',
                    app.onConfirmQuit, 
                    'Sulge rakendus', 
                    'Sulge,Tagasi');
            } else {
                window.history.back();
            }
        },
        onConfirmQuit: function(button) {
            if(button == 1) {
                navigator.app.exitApp();
            }
        }
    };
              
    app.initialize();
});