define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),

        Client = Backbone.Model.extend({

            urlRoot: "http://www.teooria.ee/api/v1/clients",

            initialize: function () {
                this.reports = new ClientCollection();
                this.reports.url = this.urlRoot; // + "/reports/" + this.id;
            },

            getAll: function() {
                var model = this
                $.ajax({
                    url: this.urlRoot,
                    success: function(data) {
                        console.log(data);
                    }
                });
            }

        }),

        ClientCollection = Backbone.Collection.extend({

            model: Client,

            url: "http://www.teooria.ee/api/v1/clients"

        });

    return {
        Client: Client,
        ClientCollection: ClientCollection
    };

});