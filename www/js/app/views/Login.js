define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        Teacher             = require('app/models/teacher'),
        tpl                 = require('text!tpl/Login.html'),
        Translate           = require('trans/translate'),

        template = _.template(tpl);

    return Backbone.View.extend({

        translate: new Translate(),

        initialize:function () {
            // this.render();
        },

        events: {
            'click #loginButton'                : 'login',
            'keyup #inputEmail, #inputPassword' : 'hideErrorMsg'
        },

        render: function () {
            this.$el.html(template({'loginName': window.localStorage.loginName, 'passw': window.localStorage.passw}));

            var translate = new Translate();
            var labels = this.$('.translate');
            // console.log(labels); 
            translate.setTranslations(labels);
            return this;
        },

        login: function (event) {
            event.preventDefault();

            var self = this;

            $('.alert-error').hide();

            var url = 'http://www.teooria.ee/api/v1/login';
            var formValues = {
                email: $('#inputEmail').val().trim(),
                password: $('#inputPassword').val().trim(),
                rememberMe: $('#rememberMe').is(':checked') ? 1 : 0
            };
            if(!$('#inputEmail').val() || !$('#inputPassword').val()) {
                $('.alert-error').text(this.translate.trans('E-posti aadress ja/või parool pole sisestatud')).show();
                return false;
            }
            
            var teacher = new Teacher();
            $.ajax({
                url: url,
                data: formValues,
                success: function (data) {
                    if(data.error) { 
                        $('.alert-error').text(data.error.text).show();
                        $('#inputPassword').val('');
                    }
                    else if(data.api_token) {
                        self.rememberMe(formValues);
                        teacher.set({
                            teacherId: data.structure_id,
                            schoolId: data.school_id,
                            schoolName: data.school_name,
                            firstname: data.firstname,
                            lastname: data.lastname,
                            name: data.name,
                            email: data.email,
                            duration: data.driving_duration,
                            apiKey: data.api_token
                        });
                        window.location.href = '#home';
                        window.sessionStorage.teacher = JSON.stringify(teacher.attributes);
                    }
                }
            });
        },

        rememberMe: function(formValues) {
            console.log(formValues);
            if(formValues.rememberMe == 0 && typeof window.localStorage.loginName !== 'undefined') {
                window.localStorage.removeItem('loginName');
                window.localStorage.removeItem('passw');
            } else if(formValues.rememberMe == 1) {
                window.localStorage.loginName = formValues.email;
                window.localStorage.passw = formValues.password;
            }
        },

        hideErrorMsg: function(event) {
            $('.alert-error').hide();
        }

    });
});

