define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/Driving.html'),
        Translate           = require('trans/translate'),

        template = _.template(tpl);

    return Backbone.View.extend({

        translate: new Translate(),

        initialize: function () {
            // this.render();
        },

        render: function () {
            this.$el.html(template());

            var translate = new Translate();
            var labels = this.$('.translate');
            // console.log(labels);
            translate.setTranslations(labels);
            return this;
        },

        events: {
            'click .driving-item'   : 'openActions',
        },

        hideOtherBoxes: function(div) {

            if($('#driving-details-container').is(':visible') && div != '#driving-details-container') {
                $('#driving-details-container').slideUp();
            }
            
            if($('#signature-pad').is(':visible')  && div != '#signature-pad') {
                 $('#signature-pad').slideUp();
            }

            if($('.actions-box').is(':visible') && div != '.actions-box') {
                $('.actions-box').slideUp();
            }

            if($('.registering-form').is(':visible') && div != '.registering-form') {
                $('.registering-form').slideUp();
            }
            $(div).slideDown();
        },

        resetActionBox: function() {
            $('.actions-box > input.drivingID').val('');
            $('.actions-box > input.ledgerID').val('');
            $('.actions-box > input.clientPhone').val('');
            $('.actions-box > input.clientEmail').val('');
            $('.actions-box > input.clientPhoto').val('');
            $('.actions-box > .profile-setting').find('.action:eq(0)').attr('id', 'Dummy1').text('VALIK1').addClass('hidden');
            $('.actions-box > .profile-setting').find('.action:eq(1)').attr('id', 'Dummy2').text('VALIK2').addClass('hidden');
            $('.actions-box > .profile-setting').find('.cancel:eq(0)').attr('id', 'Cancel1').text('TÜHISTA1').addClass('hidden');
            $('.actions-box > .profile-setting').find('.cancel:eq(1)').attr('id', 'Cancel2').text('TÜHISTA2').addClass('hidden');
            $('.actions-box > .profile-setting .actions').unbind('click');
            $('.actions-box > .profile-setting > .actions-box-header').empty();
            $('#forward').text('EDASI');
            $('#forward').removeClass('hidden');
        },

        openActions: function(event) {
            
            this.resetActionBox();
            var dataObj = $(event.currentTarget).data();
            dataObj['row'] = event.currentTarget;
            // console.log(dataObj);
            window.image = dataObj.clientPhoto;
            window.clientId = dataObj.clientID;
            // console.log(window.clientId);
            $('.actions-box').data(dataObj);
            var status = $(event.currentTarget).data('status');
            var self = this;
            // console.log(status);
            if(status == 0 || status == 1) {
                $('.actions-box').find('#Dummy1').attr('id', 'StartDriving').text('ALUSTA SÕITU').removeClass('hidden');
                $('.actions-box').find('#Cancel1').attr('id', 'CancelBefore').text('TÜHISTA SÕIT').removeClass('hidden');
                $('#CancelBefore').click(function() {
                    self.cancelDrivingBefore();
                });
                $('.actions-box').find('#Cancel2').attr('id', 'DrivingUndone').text('ÕPILANE EI ILMUNUD KOHALE').removeClass('hidden')
                $('#DrivingUndone').click(function() {
                    self.setDrivingUndone();
                });
                $('.actions-box').find('#Dummy2').attr('id', 'OpenClient').text('AVA ÕPILANE').removeClass('hidden');
            } else if(status == 2) {
                $('.actions-box').find('#Dummy1').attr('id', 'ViewDriving').text('VAATA SÕIDU DETAILE').removeClass('hidden');
                $('#ViewDriving').click(function() {
                    self.viewDriving();
                });

                $('.actions-box').find('#Dummy2').attr('id', 'OpenClient').text('AVA ÕPILANE').removeClass('hidden');
            } else if(status == 3) {
                $('.actions-box').find('#Dummy2').attr('id', 'OpenClient').text('AVA ÕPILANE').removeClass('hidden');
            } else if(status == 4) {
                $('.actions-box').find('#Dummy1').attr('id', 'ConfirmDriving').text('KINNITA SÕIT').removeClass('hidden');
                $('#ConfirmDriving').click(function() {
                    self.confirmDriving();
                });
                $('.actions-box').find('#Cancel1').attr('id', 'CancelBefore').text('TÜHISTA SÕIT').removeClass('hidden');
                $('#CancelBefore').click(function() {
                    self.cancelDrivingBefore();
                });
            }
            
            $('.actions-box').find('#Dummy2').attr('id', 'OpenClient').text('AVA ÕPILANE');
            $('.actions-box').find('#CancelDriving').text('TÜHISTA').addClass('hidden');

            $('.actions-box > input.drivingID').val($(event.currentTarget).data('drivingID'));
            // $('.actions-box > input.clientID').val($(event.currentTarget).data('clientID'));
            $('.actions-box > input.ledgerID').val($(event.currentTarget).data('ledgerID'));
            $('.actions-box > input.clientPhone').val($(event.currentTarget).data('clientPhone'));
            $('.actions-box > input.clientEmail').val($(event.currentTarget).data('clientEmail'));
            $('.actions-box > input.timestamp').val($(event.currentTarget).data('timestamp'));

            var studentName = $(event.currentTarget).data('clientName');
            var datetime = $(event.currentTarget).data('datetime');
            $('.actions-box > .profile-setting > .actions-box-header').html('<h1>' + studentName + '</h1><p class="date">' + datetime + '</p>');
            $('.actions-box > .profile-setting > .image > a > img').attr('src', window.image);
            // console.log(window.image);

            this.hideOtherBoxes('.actions-box');
        },

        viewDriving: function() {
            $('#ViewDriving').unbind('click');
            var drivingId = $('.actions-box > input.drivingID').val();
            var self = this;
            $.ajax({
                type: 'POST',
                url: 'http://www.teooria.ee/api/v1/drivings/' + drivingId,
                success: function(data) {
                    // console.log(data);
                    if(data.status == "OK") {
                        $('#driving-details-container > h1 > span.lesson-no').text(data.data.driving.lessonNo);
                        $('#driving-details-container > p.student-name').text(data.data.client.name);
                        $('#driving-details-container > .details > p.date').text(data.data.driving.formattedDate);
                        $('#driving-details-container > .details > p.place').text(data.data.driving.place);
                        $('#driving-details-container > h2.topic').text(data.data.driving.lessonTopic);
                        $('#driving-details-container > p.comments').text(data.data.driving.lessonComments);
                        
                        self.hideOtherBoxes('#driving-details-container');
                    }
                    
                   
                },
                error: function(data) {
                    // console.log(data);
                }
            });
            
        },

        confirmDriving: function() {
            $('#ConfirmDriving').unbind('click');
            var self = this;
            if(typeof(window.sessionStorage.activeDrivingId) !== 'undefined') {
                navigator.notification.alert(
                'Sõit on pooleli. \nLõpetage aktiivne sõit, et uut sõitu kinnitada.',
                null,
                'Sõit on pooleli',
                'Olgu'
            );
                return false;
            }

            //Koostatakse allkirjastamise kast/vorm
            $('#signature-pad > .clear-signature').removeClass('hidden');
            $('#signature-pad > .signature').removeClass('hidden');
            $('#signature-pad > .lesson-topic').removeClass('hidden');
            $('#signature-pad > .evaluation').removeClass('hidden');
            $('#signature-pad > #confirmBack').removeClass('hidden');
            $('#signature-pad > .start-time-container').removeClass('hidden');
            $('.error').addClass('hidden');
            $('#signature-pad > driving-details').remove();

            $('#signature-pad > h1').text('Sõidu lõpetamine');
            $('#signature-pad > .signature-label').text('Õpetaja allkiri').removeClass('hidden');
            $('#signature-pad > #forward').val('teacher');
            $('.evaluation > input[name="grade"]').prop('checked', false);
            $('#signature-pad > .lesson-topic > select').val('');

            var drivingId = $('.actions-box > input.drivingID').val();
            $('#signature-pad > input.drivingID').val(drivingId);

            $.ajax({
                type: 'POST',
                url: 'http://www.teooria.ee/api/v1/topics',
                success: function(data) {
                    // console.log(data);
                    $('.lesson-topic > select').empty();

                    if(data.status == 'OK') {
                        $('.lesson-topic > select').append('<option value>Vali teema</option>');
                        $.each(data.data, function(index, topic) {
                            // console.log(date);
                            $('.lesson-topic > select').append('<option value="' + topic.name + '">' + topic.name + '</option>');
                        });
                        self.hideOtherBoxes('#signature-pad');
                        return false;
                    } else {
                        $('.lesson-topic > select').append('<option value>Teemad puuduvad</option>');
                    }

                },
                error: function(data) {
                    // console.log(data);
                }
            });
            
            //Initializing start time container
            var today = $('.actions-box > input.timestamp').val().split(' ');
            $('#signature-pad > .start-time-container > select').val(today[1]);
            $('#signature-pad > .start-time-container > input.datetime').val(today[0] + ' ' + today[1]);

            // console.log(today[1]);
            $.ajax({
                type: 'POST',
                url: 'http://www.teooria.ee/api/v1/drivings/availableTimes',
                data: {date: today[0]},
                success: function(data) {
                    // console.log(data);
                    $('#signature-pad > .start-time-container > select').empty();
                    $('#signature-pad > .start-time-container .correct-time > span').text(data.data[0]);
                    if(data.status == 'OK') {
                        var hour = parseInt(today[1].split(':')[0]);
                        var minute = today[1].split(':')[1];
                        var newToday = hour + ':' + minute;
                        $('#signature-pad > .start-time-container > select').append('<option value>Vali aeg</option>');
                        $.each(data.data, function(index, time) {
                            var timeParts = today[1].split(':');
                            var formattedTime = parseInt(timeParts[0]) + ':' + timeParts[1];
                            // console.log(formattedTime);
                            if(formattedTime == time) {
                                $('#signature-pad > .start-time-container > select').append('<option value="' + time + '" selected>' + time + '</option>');
                            } else {
                                $('#signature-pad > .start-time-container > select').append('<option value="' + time + '">' + time + '</option>');
                            }

                            if(hour == parseInt(time.split(':')[0]) && minute != time.split(':')[1]) {
                                $('#signature-pad > .start-time-container > select').append('<option value="' + newToday + '" selected>' + newToday + '</option>');
                            }
                            
                        });
                        if($('#signature-pad > .start-time-container > select').val() == '') {
                            $('#signature-pad > .start-time-container > select > option:nth-child(2)').attr('selected', 'selected');
                        }
                    } else {
                        $('#signature-pad > .start-time-container > select').html('<option value>Ajad puuduvad</option>');
                    }

                }
            });
           
        },

        // cancelOptions: function() {
        //     $('#CancelOptions').unbind('click');
        //     var header = $('.actions-box > .profile-setting > .actions-box-header').html(); 
        //     var self = this;

        //     this.resetActionBox()

        //     $('.actions-box > .profile-setting > .actions-box-header').html(header); 

        //     $('.actions-box').find('#Dummy1').addClass('hidden');
        //     $('.actions-box').find('#Dummy2').addClass('hidden');

        //     $('.actions-box').find('.cancel').attr('id', 'CancelBefore').text('TÜHISTA SÕIT').removeClass('hidden');
        //     $('#CancelBefore').click(function() {
        //         self.cancelDrivingBefore();
        //     });

        //     $('.actions-box').find('.cancel').clone().addClass('extra').attr('id', 'DrivingUndone').text('MÄRGI SÕIT MITTETOIMUNUKS').removeClass('hidden').appendTo('.actions-box > .profile-setting > .actions');
        //     $('#DrivingUndone').click(function() {
        //         self.setDrivingUndone();
        //     });

        // },

        cancelDrivingBefore: function() {
            var self = this;
            navigator.notification.confirm('Soovite sõitu tühistada?', 
                                            self.cancelDrivingBeforeOnConfirm, 
                                            'Tühista sõit', 
                                            'Tühista, Tagasi');
            
        },

        cancelDrivingBeforeOnConfirm: function(button) {
            if(button == 1) {
                $('#CancelBefore').unbind('click');
                    var drivingID = $('.actions-box').data('drivingID');
                    var row = $('.actions-box').data('row');
                    $.ajax({
                        type: 'POST',
                        url: 'http://www.teooria.ee/api/v1/drivings/cancelDriving/' + drivingID,
                        success: function(data) {
                            console.log(data);
                            if(data.status == 'OK') {
                                $('.actions-box').slideUp();
                                $('#Drivings').ajax.reload(null, false);
                            }
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
            }
        },

        setDrivingUndone: function() {
            var self = this;
            navigator.notification.confirm('Soovite märkida sõidu mittetoimunuks?', 
                                            self.setDrivingUndoneOnConfirm, 
                                            'Õpilane ei ilmunud tundi', 
                                            'Märgi, Tagasi');
        },

        setDrivingUndoneOnConfirm: function(button) {
            if(button == 1) {
                $('#DrivingUndone').unbind('click');
                var drivingID = $('.actions-box').data('drivingID');
                var row = $('.actions-box').data('row');
                $.ajax({
                    type: 'POST',
                    url: 'http://www.teooria.ee/api/v1/drivings/setUndone/' + drivingID,
                    success: function(data) {
                        console.log(data);
                        if(data.status == 'OK') {
                            $('.actions-box').slideUp();
                            $('#Drivings').ajax.reload(null, false);
                        }
                        
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
        }


    });

});