define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/Calendar.html'),
        JqueryUI            = require('jquery-ui'),
        Translate           = require('trans/translate'),
        Moment              = require('moment'),
        // LangEE              = require('ee-est'),
        

        template = _.template(tpl);

    return Backbone.View.extend({

        translate: new Translate(),

        initialize: function () {
        },

        render: function () {
            this.$el.html(template());

            var translate = new Translate();
            var labels = this.$('.translate');
            // console.log(labels);
            translate.setTranslations(labels);
            return this;
        },

        events: {
            'change .category, .gearbox, #placeDropdown' : 'filterChanged',
            'keyup .places-combobox-input'               : 'filterChanged',
            'click .copy-last-events'                    : 'copyLastWeekEvents',
        },

        filterChanged: function(event) {
            $('#calendar').fullCalendar('refetchEvents');
            $('.place-error-msg').hide();
            
            window.localStorage.category = $('.category').val();
            window.localStorage.gearbox = $('.gearbox').val();
            window.localStorage.place = $('#placeDropdown').val() != '' ? $('#placeDropdown').val() : $('.places-combobox-input').val();
            // console.log($('#placeDropdown').val());
            // console.log($('.places-combobox-input').val());
            // console.log('changed' + event.currentTarget);
        },

        copyLastWeekEvents: function(event) {
            var dates = this.getCalendarPreviousWeekDateRange()
            var teacherID = (JSON.parse(window.sessionStorage.teacher)).teacherId;

            $.ajax({
                type: 'POST',
                url: 'http://www.teooria.ee/api/v1/drivings/copyLastWeek/' + teacherID,
                data: dates,
                success: function(data) {
                    console.log(data);
                    if(data.status == 'OK') {
                        $('#calendar').fullCalendar( 'refetchEvents' );
                    } else if(data.status.error == 'ERROR') {
                        navigator.notification.alert('Ei saanud kopeerimist teostada. \nProovi hiljem uuesti', 
                                                                null, 
                                                                'Tõrge', 
                                                                'Sulge');
                    }
                }, 
                error: function() {
                    navigator.notification.alert('Ilmnes tõrge. \nProovi hiljem uuesti', 
                                                                null, 
                                                                'Tõrge', 
                                                                'Sulge');
                }
            });
        },

        getCalendarPreviousWeekDateRange: function() {
            var calendar = $('#calendar').fullCalendar('getCalendar');
            var view = calendar.view;
            var start = Moment(view.start).subtract(7, 'days').format('YYYY-MM-DD HH:mm:ss');
            var end = Moment(view.end).subtract(7, 'days').format('YYYY-MM-DD HH:mm:ss');
            var dates = { start: start, end: end };
            return dates;
        }

    });

});