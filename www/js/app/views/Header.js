define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/fragments/header.html'),
        Translate           = require('trans/translate'),

        template = _.template(tpl);

    return Backbone.View.extend({

        translate: new Translate(),
        // startTime: 0,
        // endTime: 0,

        initialize: function () {
            // this.render();
        },

        render: function () {
            this.$el.html(template);

            var translate = new Translate();
            var labels = this.$('.translate');
            // console.log(labels);
            translate.setTranslations(labels);
            return this;
        },

        events : {
            'click #logout'             : 'logout',
            'click .exitButton'         : 'closeApplication',
            'click .nav-trigger'        : 'triggerMenu',
            'click .dashboard-item'     : 'closeMenu'
        },

        logout: function(event) {
            if(window.sessionStorage.activeDrivingId !== undefined) {
                navigator.notification.confirm(
                    'Välja logimisel ei salvestata poolelioleva sõidu andmeid.',
                    this.onConfirmLogout,
                    'Sõit pooleli',
                    'Logi välja,Tagasi'
                );
            } else {
                window.sessionStorage.clear();
                Backbone.history.navigate("#login", {trigger: true, replace: false}); 
            }
            
        },
        closeApplication: function(event) {
            // this.startTime = (new Date()).getTime();
            navigator.notification.alert(
                'Soovite rakendusest väljuda?',
                this.onConfirmQuit,
                'Sulge rakendus',
                'Sulge,Tagasi'
            );
            // this.endTime = (new Date()).getTime();
            // $('.geolocation-log').append('<p class="succeess">[CONFIRM] startTime=' 
            //     + this.startTime + ', endTime=' + this.endTime + ', endTime-startTime=' 
            //     + (this.endTime-this.startTime) + '</p>');
        },

        triggerMenu: function(event) {
            $('.navi').slideToggle();
        },

        onConfirmQuit: function(button) {
           if(button == 1) {
                navigator.app.exitApp(); 
            }
        },

        onConfirmLogout: function(button) {
            if(button == 1) {
                navigator.geolocation.clearWatch(window.sessionStorage.activeWatchId);
                window.sessionStorage.clear(); 
                Backbone.history.navigate("#login", {trigger: true, replace: false});
                window.plugins.insomnia.allowSleepAgain();
            }
        },

        closeMenu: function(event) {
            $('.navi').slideUp();
        }

    });

});