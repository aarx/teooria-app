define(function (require) {

    "use strict";

    var $            = require('jquery'),
        _            = require('underscore'),
        Backbone     = require('backbone'),
        tpl          = require('text!tpl/Dashboard.html'),
        Translate    = require('trans/translate'),
        
        template = _.template(tpl);


    return Backbone.View.extend({

        initialize: function () {
           // this.render();
        },

        render: function () {
            this.$el.html(template());

            var translate = new Translate();
            var labels = this.$('.translate');
            // console.log(labels);
            translate.setTranslations(labels);
            return this;
        }

    });

});