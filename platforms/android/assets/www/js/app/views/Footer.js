define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/fragments/footer.html'),
        Translate           = require('trans/translate'),

        template = _.template(tpl),
        positionCounter = 0;

    return Backbone.View.extend({

        translate: new Translate(),
        testMode: true, // Allow measuring time for geolocation and notifications

        initialize: function () {
            // this.render();
        },

        render: function () {
            var teacher = (typeof(window.sessionStorage.teacher) !== "undefined") ? JSON.parse(window.sessionStorage.teacher) : {name: 'ERROR', schoolName: 'ERROR2' };
            var label = teacher.name + ' - ' + teacher.schoolName;
            var activeStudent = (typeof(window.sessionStorage.activeStudent) !== "undefined") ? window.sessionStorage.activeStudent : "";
            var activeStartTime = (typeof(window.sessionStorage.activeStartTime) !== "undefined") ? window.sessionStorage.activeStartTime : "";
            var activeDrivingClock = (typeof(window.sessionStorage.activeDrivingClock) !== "undefined") ? window.sessionStorage.activeDrivingClock : "";
            var activeDrivingId = (typeof(window.sessionStorage.activeDrivingId) !== "undefined") ? window.sessionStorage.activeDrivingId : undefined;
            
            
            this.$el.html(template({'teacherLabel': label , 'activeStudent': activeStudent, 'activeStartTime': activeStartTime, 'activeDrivingClock': activeDrivingClock, 'activeDrivingId' : activeDrivingId}));

            var translate = new Translate();
            var labels = this.$('.translate');
            // console.log(labels);
            translate.setTranslations(labels);
            return this;
        },

        events: {
            'click .active-driving'              : 'activeDrivingOptions',
            'click #FinishDriving'               : 'finishDriving',
            'click #forward'                     : 'confirmWindow',
            'click #StartDriving'                : 'startDriving',
            // 'click #CancelDriving'               : 'cancelDriving',
            'click #OpenClient'                  : 'openClient',
            'click #RegisterNewDriving'          : 'registerDrivingForm',
            'click #SaveNewDriving'              : 'saveNewDriving',
            'click #confirmBack'                 : 'confirmWindowBack',
            'click .correct-time'                : 'confirmWindowCorrectTime'
        },

        resetActionBox : function() {
            // $('.actions-box > input.drivingID').val('');
            // $('.actions-box > input.ledgerID').val('');
            // $('.actions-box > input.clientPhone').val('');
            // $('.actions-box > input.clientEmail').val('');
            // $('.actions-box > input.clientPhoto').val('');
            $('.actions-box > .profile-setting').find('.action:eq(0)').attr('id', 'Dummy1').text('VALIK1').addClass('hidden');
            $('.actions-box > .profile-setting').find('.action:eq(1)').attr('id', 'Dummy2').text('VALIK2').addClass('hidden');
            $('.actions-box > .profile-setting').find('.cancel:eq(0)').attr('id', 'Cancel1').text('TÜHISTA1').addClass('hidden');
            $('.actions-box > .profile-setting').find('.cancel:eq(1)').attr('id', 'Cancel2').text('TÜHISTA2').addClass('hidden');
            $('.actions-box > .profile-setting .actions').unbind('click');
            $('.actions-box > .profile-setting > .actions-box-header').empty();
            $('#forward').text('EDASI');
            $('#forward').removeClass('hidden');
            
            $('.various-tasks').removeClass('opened');
        },

        hideOtherBoxes: function(div) {
            if($('#driving-details-container').is(':visible') && div != '#driving-details-container') {
                $('#driving-details-container').slideUp();
            }
            
            if($('#signature-pad').is(':visible')  && div != '#signature-pad') {
                 $('#signature-pad').slideUp();
            }

            if($('.actions-box').is(':visible') && div != '.actions-box') {
                $('.actions-box').slideUp();
            }

            if($('.registering-form').is(':visible') && div != '.registering-form') {
                $('.registering-form').slideUp();
            }
            $(div).slideDown();
        },

        activeDrivingOptions: function() {
            var self = this;
            this.resetActionBox();
            $('.actions-box > .profile-setting').find('#Dummy1').attr('id', 'FinishDriving').text('LÕPETA SÕIT').removeClass('hidden');
            $('.actions-box > .profile-setting').find('#Cancel1').attr('id', 'CancelDriving').text('TÜHISTA').removeClass('hidden');
            $('#CancelDriving').click(function() {
                    self.cancelDriving();
                });
            $('actions-box > input.drivingID').val(window.sessionStorage.activeDrivingId);
            $('.actions-box > .profile-setting > .image > a > img').attr('src', window.sessionStorage.activeStudentPhoto);

            
            var studentName =  window.sessionStorage.activeStudent;
            var datetime =  window.sessionStorage.activeDrivingDatetimeFormatted;
            $('.actions-box > .profile-setting > .actions-box-header').html('<h1>' + studentName + '</h1><p>' + datetime + '</p>');

            this.hideOtherBoxes('.actions-box');
        },

        startDriving: function() {
            //Sõidu alustuse valideerimine: aktiivne sõit
            if(window.sessionStorage.activeStudent) {
                this.onConfirmFinishDriving(1);
                // navigator.notification.alert('Sõit on pooleli. Palun lõpetage enne käimasolev sõit.', 
                //     null, 
                //     'Lõpetage aktiivne sõit', 
                //     'Olgu');

                return false;
            }

            $('.actions-box').slideUp();

            var current = new Date();
            window.sessionStorage.activeStartTime = 
                                    ((current.getHours() < 10) ? "0" + current.getHours() : current.getHours()) + 
                                    ":" + 
                                    ((current.getMinutes() < 10) ? "0" + current.getMinutes() : current.getMinutes());
                                    
            window.sessionStorage.activeCurrentDate = current;
           
            $('.active-driving > #StartTime').text('Alustatud ' + window.sessionStorage.activeStartTime);

            window.sessionStorage.activeStudent = $('.actions-box-header > h1').text();

            window.sessionStorage.activeStudentPhoto = $('.actions-box > .profile-setting > .image > a > img').attr('src');
            
            $('.active-driving > #ClientName').text('Aktiivne sõit: ' +  window.sessionStorage.activeStudent);

            this.startTimer();

            window.sessionStorage.activeDrivingId = $('.actions-box > input.drivingID').val();
            
            $('.active-driving > input.drivingID').val(window.sessionStorage.activeDrivingId);

            $('.active-driving').removeClass('hidden');

            positionCounter = 0;
            
            var dataObj = $('.actions-box').data();
            window.sessionStorage.activeDrivingDatetime = dataObj.timestamp;
            window.sessionStorage.activeDrivingDatetimeFormatted = dataObj.datetime;

            $('#signature-pad, .registering-form, #driving-details-container').addClass('padding-bottom');

            // call geolocation
            var self = this;
            window.sessionStorage.activeIntervalId = setInterval( function () {
                    if(self.testMode) window.sessionStorage.geolocationStartTime = (new Date()).getTime();
                    if((JSON.parse(window.sessionStorage.teacher)).schoolId == '155') $('.geolocation-log').append('<span>Geolocation interval run...</span>');
                    self.getGeolocation();
                }, 
                8000 //check every 8 seconds
            );

            
            // // For showing current speed
            // var options = { /*maximumAge: 1000, timeout: 20000,*/ enableHighAccuracy: true };
            // window.sessionStorage.activeDrivingSpeedId = navigator.geolocation.watchPosition(this.onSpeedSuccess, this.onSpeedError, options);

            // window.sessionStorage.activeWatchId = setInterval(function() { 
            //     if(this.testMode) window.sessionStorage.geolocationStartTime = (new Date()).getTime();
            //     if((JSON.parse(window.sessionStorage.teacher)).schoolId == '155') $('.geolocation-log').append('<span>Geolocation interval run...</span>');

            //     navigator.geolocation.getCurrentPosition(self.onSuccess, self.onError, options);
            // }, 10000);
        },

        onConfirmFinishDriving: function(button) {
            if(button == 1) {
                this.activeDrivingOptions();
            }
        },

        startTimer: function() {
            var start = new Date(window.sessionStorage.activeCurrentDate);

            window.sessionStorage.activeTimerId = setInterval(function() {
                window.sessionStorage.activeDrivingClock = $('#TimerPlaceholder').text(function(){
                    var diff = Math.round((new Date() - start) / 1000, 0);
                    if((diff / 3600) > 1) {
                        return Math.round((diff / 3600), 0) + ' h ' + Math.round((diff % 3600), 0) + ' min';
                    } else if(diff > 59) {
                        return Math.round((diff / 60), 0) + ' min';
                    } else {
                        return diff + ' sek ';
                    }
                }).text();
            }, 1000);
        },

        finishDriving: function(event) {

            var self = this;
            
            $('.actions-box').slideUp();
            $('.active-driving').addClass('hidden');
            

            //Koostatakse allkirjastamise kast/vorm
            $('#signature-pad > .clear-signature').removeClass('hidden');
            $('#signature-pad > .signature').removeClass('hidden');
            $('#signature-pad > .lesson-topic').removeClass('hidden');
            $('#signature-pad > .evaluation').removeClass('hidden');
            $('#signature-pad > #confirmBack').removeClass('hidden');
            $('#signature-pad > .start-time-container').removeClass('hidden');
            $('.evaluation-error-msg').addClass('hidden');
            $('.topic-error-msg').addClass('hidden');
            $('.start-time-error-msg').addClass('hidden');
            $('#signature-pad > driving-details').remove();

            $('#signature-pad > h1').text('Sõidu lõpetamine');
            $('#signature-pad > .signature-label').text('Õpetaja allkiri').removeClass('hidden');
            $('#signature-pad > #forward').val('teacher');
            $('.evaluation > input[name="grade"]').prop('checked', false);
            $('#signature-pad > .lesson-topic > select').val('');

            

            this.hideOtherBoxes('#signature-pad');

            // Initializing topics dropdown
            // If already inialized, then use existing ones
            if($('.lesson-topic > select').length == 1) {
                console.log($('.lesson-topic > select').length);
                $.ajax({
                    type: 'POST',
                    url: 'http://www.teooria.ee/api/v1/topics',
                    success: function(data) {
                        // console.log(data);
                        $('.lesson-topic > select').empty();

                        if(data.status == 'OK') {
                            $('.lesson-topic > select').append('<option value>Vali teema</option>');
                            $.each(data.data, function(index, topic) {
                                $('.lesson-topic > select').append('<option value="' + topic.name + '">' + self.translate.trans(topic.name) + '</option>');
                            });
                        } else {
                            $('.lesson-topic > select').html('<option value>Teemad puuduvad</option>');
                        }

                    }
                });
            }
           

            //Initializing start time container
            var today = window.sessionStorage.activeDrivingDatetime.split(' ');
            $('#signature-pad > .start-time-container input.start-time').val(today[1]);
            $('#signature-pad > .start-time-container input.datetime').val(today[0] + ' ' + today[1]);
            // console.log(today[0]);
            $.ajax({
                type: 'POST',
                url: 'http://www.teooria.ee/api/v1/drivings/availableTimes',
                data: {date: today[0]},
                success: function(data) {
                    console.log(data);
                    var hour = parseInt(today[1].split(':')[0]);
                    var minute = today[1].split(':')[1];
                    var newToday = hour + ':' + minute;
                    $('#signature-pad > .start-time-container > select').empty();
                    $('#signature-pad > .start-time-container .correct-time > span.suggested-time').text(data.data[0]);
                    if(data.status == 'OK') {
                        $('#signature-pad > .start-time-container > select').append('<option value>Vali aeg</option>');
                        // console.log(today[1]);
                        $.each(data.data, function(index, time) {
                            var timeParts = today[1].split(':');
                            var formattedTime = parseInt(timeParts[0]) + ':' + timeParts[1];
                            // console.log(formattedTime);
                            if(formattedTime == time) {
                                $('#signature-pad > .start-time-container > select').append('<option value="' + time + '" selected>' + time + '</option>');
                            } else {
                                $('#signature-pad > .start-time-container > select').append('<option value="' + time + '">' + time + '</option>');
                            }
                            
                            if(hour == parseInt(time.split(':')[0]) && minute != time.split(':')[1]) {
                                $('#signature-pad > .start-time-container > select').append('<option value="' + newToday + '" selected>' + newToday + '</option>');
                            }
                            
                        });
                    } else {
                        $('#signature-pad > .start-time-container > select').html('<option value>Ajad puuduvad</option>');
                    }

                }
            });
        },

        confirmWindow: function(event) {

            var mode = $(event.currentTarget).val();
            var evaluation = $('#signature-pad > .evaluation > input[name=grade]:checked').val();
            var topic = $('#signature-pad > .lesson-topic > select').val();
            var startingTime = $('#signature-pad > .start-time-container > select').val();
            var validated = true;
            $('.error').addClass('hidden');

            if(mode == 'teacher') {
                
            
                if(evaluation === undefined) {
                    $('.evaluation-error-msg').removeClass('hidden');
                    validated = false;
                } 

                if(topic === '') {
                    $('.topic-error-msg').removeClass('hidden');
                    validated = false;
                } 

                if(startingTime === '') {
                    $('.start-time-error-msg').removeClass('hidden');
                    validated = false;
                } 

                if(document.signaturePad.isEmpty()) {
                    $('.signature-error-msg').removeClass('hidden');
                    validated = false;
                }

                if(!validated) {
                    return false;
                }
                window.sessionStorage.activeDrivingTeacherSign = document.signaturePad.toDataURL();

                window.sessionStorage.activeDrivingTopic = topic;
                window.sessionStorage.activeDrivingGrade = $('.evaluation > input[name="grade"]:checked').val();
                window.sessionStorage.activeDrivingClock = $('.active-driving #TimerPlaceholder').text();
                $('.active-driving > #TimerPlaceholder').text('');

                document.signaturePad.clear();

                $('#signature-pad > .lesson-topic').addClass('hidden');
                $('#signature-pad > .evaluation').addClass('hidden');
                $('#signature-pad > .signature-label').text('Õpilase allkiri');
                $('#signature-pad > .start-time-container').addClass('hidden');
                $(event.currentTarget).val('student');

            } else if(mode == 'student') {

                if(document.signaturePad.isEmpty()) {
                    $('.signature-error-msg').removeClass('hidden');
                    return false;
                }

                //Sõidu andmete võtmine

                window.sessionStorage.activeDrivingStudentSign = document.signaturePad.toDataURL();

                document.signaturePad.clear();
                clearInterval(window.sessionStorage.activeTimerId);

                //Prparing data variables for server connection
                
                var dateHolder = ($('#signature-pad > .start-time-container > select').val() != '') ? $('#signature-pad > .start-time-container > select').val() : undefined;
                console.log(dateHolder);
                var drivingData = {
                    drivingId   : (typeof(window.sessionStorage.activeDrivingId) === 'undefined') ? $('#signature-pad > input.drivingID').val() : window.sessionStorage.activeDrivingId,
                    datetime    : (dateHolder !== undefined) ?$('#signature-pad > .start-time-container > input.datetime').val() : dateHolder,
                    startTime   : (dateHolder !== undefined) ? $('#signature-pad > .start-time-container > select').val() : dateHolder,
                    grade       : window.sessionStorage.activeDrivingGrade,
                    topic       : window.sessionStorage.activeDrivingTopic,
                    teacherSign : window.sessionStorage.activeDrivingTeacherSign,
                    studentSign : window.sessionStorage.activeDrivingStudentSign,
                    doneStatus  : 'completed'
                };


                $(event.currentTarget).val('confirm');
                $(event.currentTarget).text('SULGE');

                
                
                this.confirmDriving(drivingData);

                $('#signature-pad > .signature').addClass('hidden');
                $('#signature-pad > #confirmBack').addClass('hidden');
                $('#signature-pad > .clear-signature').addClass('hidden');
                $('#signature-pad > .signature-label').addClass('hidden');

            } else if(mode == 'confirm') {
                $(event.currentTarget).val('');
                $('#signature-pad').slideUp();
                $('#signature-pad > .driving-details').addClass('hidden');
                
            } else {
                // console.log('Juhutus viga.');
            }
        },

        confirmDriving: function(driving) {
            var self = this;

            // stop positioning
            this.clearWatch();

            // save driving to localStorage
            window.localStorage.setItem('driving-' + driving.drivingId, JSON.stringify(driving));

            // collect drivings and positions
            var drivings = [];
            var key;

            for( var i = 0; i < window.localStorage.length; ++i ) {
                key = window.localStorage.key(i);
                if(key.match(/driving-/)) {

                    driving = JSON.parse(window.localStorage.getItem(key));

                    driving.positions = [];

                    for( var k = 0; k < window.localStorage.length; ++k ) {
                        key = window.localStorage.key(k);
                        if(key.match(/position-/) && (JSON.parse(window.localStorage.getItem(key))).drivingId == driving.drivingId) {
                            if(this.checkPositionObject(JSON.parse(window.localStorage.getItem(key)))) {
                                driving.positions.push(JSON.parse(window.localStorage.getItem(key)));
                            } else {
                                window.localStorage.removeItem(key);
                            }                      
                        } 
                    }

                    drivings.push(driving);
                } 
            }
            
            $.ajax({
                type: 'POST',
                url: 'http://www.teooria.ee/api/v1/drivings/confirmDriving',
                data: {drivings: drivings},
                success: function(data) {
                    if(data.status == "OK") {
                        if(typeof(window.sessionStorage.activeDrivingId) === 'undefined') {
                            $('#signature-pad > .driving-details').html('<p>Sõidutund: <strong>' + data.data[0].driving.place + ', ' + data.data[0].driving.date + ' ' + data.data[0].driving.time + '</strong> on kinnitatud.</p>');
                            $('#signature-pad > .driving-details').removeClass('hidden');
                            $('#signature-pad > h1').text('Kinnitatud');
                        } else {
                            $('#signature-pad > .driving-details').html('<p>Tunni algus: ' + $('#signature-pad .start-time-container > select').val() /*window.sessionStorage.activeStartTime*/ + '</p>' +
                                                                    '<p>Tunni kestus: ' + window.sessionStorage.activeDrivingClock + '</p>' +
                                                                    //'<p>Tunni läbisõit: SIIA TEEPIKKUS</p>' +
                                                                    '<p>Tunni hinne: ' + window.sessionStorage.activeDrivingGrade + '</p>'
                                                                    );
                            $('#signature-pad > .driving-details').removeClass('hidden');
                            $('#signature-pad > h1').text('Salvestatud');
                            $("#Drivings").DataTable().ajax.reload(null, false);
                            
                            var key;
                            var count = window.localStorage.length - 1;
                            for( var i = count; i >= 0; i-- ) {
                                key = window.localStorage.key(i);
                                if(key.indexOf('driving-') == 0 || key.indexOf('position-') == 0) {
                                    window.localStorage.removeItem(key);
                                }
                            }

                            count = window.localStorage.length - 1;
                            for( var i = count; i >= 0; i-- ) {
                                key = window.localStorage.key(i);
                                if(key.match(/position-/) && (JSON.parse(window.localStorage.getItem(key))).drivingId == window.sessionStorage.activeDrivingId) {
                                    window.localStorage.removeItem(key);
                                }
                            }
                            
                            this.positions = [];
                        }

                    } else { 
                        $('#signature-pad > h1').text('Tekkis viga');
                    }

                    self.clearActiveDriving();
                },
                error: function(data) {
                   $('#signature-pad > h1').text('Tekkis viga');
                }
            });
        },

        cancelDriving: function(event) {
            this.clearWatch();
            clearInterval(window.sessionStorage.activeTimerId);
            $('.actions-box').slideUp();
            $('.active-driving').addClass('hidden');
            this.clearActiveDriving();
        },

        clearActiveDriving: function() {
            var activePosition = 'position-' + window.sessionStorage.activeDrivingId;

            var count = window.sessionStorage.length - 1;
            for( var i = count; i >= 0; i-- ) {
                var key = window.sessionStorage.key(i);
                if(key != 'teacher') {
                    window.sessionStorage.removeItem(key);
                }
            }

            var count = window.localStorage.length - 1;
            for( var i = count; i >= 0; i-- ) {
                var key = window.localStorage.key(i);
                if(key.indexOf(activePosition) >= 0) {
                    window.localStorage.removeItem(key);
                }
            }
            $('#signature-pad, .registering-form, #driving-details-container').removeClass('padding-bottom');

            // reset TimerPlaceholder
            $('.active-driving #TimerPlaceholder').text('');

        },

        getGeolocation: function() {
            var options = {
                maximumAge: 250, 
                enableHighAccuracy: true
            };
            window.sessionStorage.activeWatchId = window.navigator.geolocation.watchPosition(this.onSuccess, this.onError, options);
            window.setTimeout(function () {
                    window.navigator.geolocation.clearWatch( window.sessionStorage.activeWatchId ) 
                }, 
                5000 //stop checking after 5 seconds
            );
        },

        onSuccess: function(position) {
            if(this.testMode) {
                window.sessionStorage.geolocationEndTime = (new Date()).getTime();

                var items = {
                    drivingId: window.sessionStorage.activeDrivingId,
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    altitude: position.coords.altitude,
                    accuracy: position.coords.accuracy,
                    heading: position.coords.heading,
                    speed: position.coords.speed,
                    positionTimestamp: position.timestamp,
                    timestamp: (new Date()).getTime(),
                    requestSent: window.sessionStorage.geolocationStartTime,
                    responseReceived: window.sessionStorage.geolocationEndTime,
                    duration: (window.sessionStorage.geolocationEndTime-window.sessionStorage.geolocationStartTime)
                };
            } else {
                // console.log(position);
                var items = {
                    drivingId: window.sessionStorage.activeDrivingId,
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    altitude: position.coords.altitude,
                    accuracy: position.coords.accuracy,
                    heading: position.coords.heading,
                    speed: position.coords.speed,
                    positionTimestamp: position.timestamp,
                    timestamp: (new Date()).getTime(),
                    requestSent: window.sessionStorage.geolocationStartTime,
                    responseReceived: window.sessionStorage.geolocationEndTime
                };
                // console.log(items);
            }

            if(typeof window.sessionStorage.activeLastSpeed === 'undefined') {
                window.sessionStorage.activeLastSpeed = items.speed;
            }
            console.log(window.sessionStorage.activeLastSpeed);

            window.localStorage.setItem('position-' + window.sessionStorage.activeDrivingId + '-' + positionCounter++, JSON.stringify(items));
            if((JSON.parse(window.sessionStorage.teacher)).schoolId == '155') $('.geolocation-log').append('<p class="succeess">' + JSON.stringify(items) + '</p>');
        
        },

        // onSpeedSuccess: function(position) {
        //     var positionObject = {};

        //     if ('coords' in position) {
        //         positionObject.coords = {};

        //         if ('latitude' in position.coords) {
        //             positionObject.coords.latitude = position.coords.latitude;
        //         }
        //         if ('longitude' in position.coords) {
        //             positionObject.coords.longitude = position.coords.longitude;
        //         }
        //         if ('accuracy' in position.coords) {
        //             positionObject.coords.accuracy = position.coords.accuracy;
        //         }
        //         if ('altitude' in position.coords) {
        //             positionObject.coords.altitude = position.coords.altitude;
        //         }
        //         if ('altitudeAccuracy' in position.coords) {
        //             positionObject.coords.altitudeAccuracy = position.coords.altitudeAccuracy;
        //         }
        //         if ('heading' in position.coords) {
        //             positionObject.coords.heading = position.coords.heading;
        //         }
        //         if ('speed' in position.coords) {
        //             positionObject.coords.speed = position.coords.speed;
        //         }
        //     }

        //     if ('timestamp' in position) {
        //         positionObject.timestamp = position.timestamp;
        //     }
        //     if(typeof window.sessionStorage.activeLastSpeed === 'undefined') {
        //         window.sessionStorage.activeLastSpeed = positionObject.coords.speed;
        //     }
        //     $('.active-driving > .speed-container > #ActiveDrivingSpeed').text(window.sessionStorage.activeLastSpeed);
        //     $('.geolocation-log').append('<span class="succeess">[BY_SPEED] ' + JSON.stringify(positionObject) + '</span>');
        //     console.log(window.sessionStorage.activeLastSpeed);
        // },

        // onSpeedError: function(position) {
        //     $('.active-driving > .speed-container > #ActiveDrivingSpeed').text(window.sessionStorage.activeLastSpeed);
        // },

        clearWatch: function() {
            if (typeof window.sessionStorage.activeWatchId !== 'undefined') {
                navigator.geolocation.clearWatch(window.sessionStorage.activeWatchId);
                // clearInterval(window.sessionStorage.activeWatchId);
                clearInterval(window.sessionStorage.activeintervalId);
                // window.sessionStorage.activeWatchId = undefined;
            }
        },

        onError: function(error) {
            var errorMessage;
            switch(error.code) {
                case error.PERMISSION_DENIED:
                    errorMessage = "User denied the request for Geolocation."
                    break;
                case error.POSITION_UNAVAILABLE:
                    errorMessage = "Location information is unavailable."
                    break;
                case error.TIMEOUT:
                    errorMessage = "The request to get user location timed out."
                    break;
                case error.UNKNOWN_ERROR:
                    errorMessage = "An unknown error occurred."
                    break;
            }

            if((JSON.parse(window.sessionStorage.teacher)).schoolId == '155') $('.geolocation-log').append('<span class="error">' + errorMessage + '</span>');
            
            // navigator.notification.confirm(
            //     'GPS on välja lülitatud.\n Sõidu teekonda ei salvestata.',
            //     null,
            //     'GPS väljas',
            //     'Tagasi'
            // );
            // navigator.vibrate(500);
        },

        openClient: function(event) {
            $('#ViewDriving').unbind('click');
            $('#ConfirmDriving').unbind('click');
            // var clientId = $('.actions-box > input.clientID').val();
            var ledgerID = $('.actions-box > input.ledgerID').val();
            var studentName = $('div.actions-box-header > h1').text();
            var email = $('.actions-box > input.clientEmail').val();
            var phone = $('.actions-box > input.clientPhone').val();
            // window.clientId = clientId;

            this.resetActionBox();
            $('.actions-box').find('#Dummy1').attr('id', 'RegisterNewDriving').text('REGISTREERI UUS SÕIT').removeClass('hidden');
            // console.log(window.image);
            $('.actions-box > input.ledgerID').val(ledgerID);

            // $('.actions-box > input.clientID').val(clientId);

            $('.actions-box > .profile-setting > .actions-box-header').html('<h1>' + studentName + '</h1><div class="btn-container"><p class="email"><a href="mailto:' + email + '"><i class="fa fa-envelope"></i></a></p><p class="phone"><a href="tel:' + phone + '"><i class="fa fa-phone-square"></i></a></p><p class="phone"><a href="sms:' + phone + '"><i class="fa fa-commenting"></i></a></p></div>');

            this.hideOtherBoxes('.actions-box');
        },

        registerDrivingForm: function(event) {

            $.ajax({
                type: 'POST',
                url: 'http://www.teooria.ee/api/v1/drivings/freeDrivingDates',
                data: { ledgerID: $('input.ledgerID').val().trim() },
                success: function(data) {
                    // console.log(data);
                    $('#timestamp').empty();

                    if(data.status == 'ERROR') {
                        $('#timestamp').append('<option value>Vabu aegu pole</option>');
                        $('#SaveNewDriving').hide();
                        return;
                    } else {
                        $('#SaveNewDriving').show();
                    }

                    $('#timestamp').append('<option value>Vali aeg</option>');


                    $.each(data.data, function(index, date) {
                        $('#timestamp').append('<option data-drivingid="' + date.drivingId + '" value="' + date.timestamp + '">' + date.displayTimestamp + ' ' + date.place + '</option>');
                    });
                   
                }
            });

            // $('#NewDrivingForm > input.clientID').val($('.actions-box > input.clientID').val());
            $('#NewDrivingForm > h1.name').text($('div.actions-box-header > h1').text());

            $('#NewDrivingForm > input.ledgerID').val($('.actions-box > input.ledgerID').val());
            
            this.hideOtherBoxes('.registering-form');
        },

        saveNewDriving: function(event) {
            event.preventDefault();
            var driving = {
                drivingId : $('#NewDrivingForm > select > option:selected').attr('data-drivingid'),
                // clientId  : $('#NewDrivingForm > input.clientID').val(),
                ledgerId  : $('#NewDrivingForm > input.ledgerID').val()
            };
            if(driving.drivingId === undefined) {
                return false;
            }

             $.ajax({
                url: 'http://www.teooria.ee/api/v1/drivings/register',
                data: driving,
                success: function(data) {
                    // console.log(data);
                    // $('#NewDrivingForm > input.clientID').val("");
                    $('#NewDrivingForm > input.ledgerID').val("");
                    $("#Drivings").DataTable().ajax.reload(null, false);
                    $('.registering-form').slideUp();

                }
            });
        },

        confirmWindowBack: function() {
            var state = $('#signature-pad > #forward').val();
            document.signaturePad.clear();
            if(state == 'teacher') {
                $('#signature-pad > input.drivingID').val('');
                $('#signature-pad').slideUp();
                if(typeof(window.sessionStorage.activeDrivingId) !== 'undefined') {
                    $('.active-driving').removeClass('hidden');
                }
            } else if(state == 'student') {
                this.finishDriving();
            } else {
                $('#signature-pad').slideUp();
            }
        },

        confirmWindowCorrectTime: function(event) {
            var suggestedTime = $('span.suggested-time', event.currentTarget).text();
            $('#signature-pad > .start-time-container > select').val(suggestedTime);
            // console.log(suggestedTime);
        },

        checkPositionObject: function(obj) {
            return (obj.longitude && obj.latitude || this.testMode) ? true : false;
        }

    });

});