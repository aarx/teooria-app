define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),

        Client = Backbone.Model.extend({

            urlRoot: "http://localhost:3000/clients",

            initialize: function () {
                this.reports = new ClientCollection();
                this.reports.url = this.urlRoot + "/" + this.id + "/reports";
            }

        }),

        ClientCollection = Backbone.Collection.extend({

            model: Client,

            url: "http://localhost:3000/clients"

        }),

        originalSync = Backbone.sync;

    Backbone.sync = function (method, model, options) {
        if (method === "read") {
            options.dataType = "jsonp";
            return originalSync.apply(Backbone, arguments);
        }
    };

    return {
        Client: Client,
        ClientCollection: ClientCollection
    };

});