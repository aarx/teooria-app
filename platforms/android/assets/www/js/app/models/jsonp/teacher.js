define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),

        Teacher = Backbone.Model.extend({

            urlRoot: "http://localhost:3000/clients",

            initialize: function () {
                console.log("Teacher jsonp initialized");
                // this.reports = new ClientCollection();
                // this.reports.url = this.urlRoot + "/" + this.id + "/reports";
            }

        }),

        originalSync = Backbone.sync;

    Backbone.sync = function (method, model, options) {
        if (method === "read") {
            options.dataType = "jsonp";
            return originalSync.apply(Backbone, arguments);
        }
    };

    return {
        Teacher: Teacher
    };

});