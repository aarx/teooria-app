define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),


        APIKey = Backbone.Model.extend({

            sync: function (method, model, options) {
                if (method === "read") {
                    findById(parseInt(this.id)).done(function (data) {
                        options.success(data);
                    });
                }
            }

        });

    return {
        APIKey: APIKey
    };

});