define(function (require) {

   

    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        
        Driving = Backbone.Model.extend({
            defaults: {
                clientID: 0,
                clientName: "",
                schoolID: 0,
                teacherID: 0,
                teacherName: "",
                date: "",
                place: "",
                lessonNo: 0,
                lessonTopic: "",
                ledgerName: "",
                status: "",
                doneStatus: "",
                studentConfirm: "",
                teacherConfirm: "",

            },
            urlRoot: "http://www.teooria.ee/api/v1/drivings",

            initialize: function () {
                this.drivings = new DrivingList();
                
                // console.log('Driving has been initialized ');
                this.on("invalid", function (model, error) {
                    // console.log("Houston, we have a problem: " + error);
                });
            }

        }),
        
        DrivingList = Backbone.Collection.extend({

            model: Driving,

            urlRoot: "http://www.teooria.ee/api/v1/drivings",
        });

    return {
        Driving: Driving,
        DrivingList: DrivingList
    };

});