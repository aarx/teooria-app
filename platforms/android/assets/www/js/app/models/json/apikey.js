define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone');

        return Backbone.Model.extend({
            defaults: {
                userId: 1,
                apiKey: 123456789
            },

            initialize: function () {
                // console.log('APIAuth inintialized');
            }

        });

});