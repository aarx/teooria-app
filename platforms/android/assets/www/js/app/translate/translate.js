define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        TranslateFile       = require('json!trans/translations.json');

        return Backbone.Model.extend({
            
            initialize: function () {
                // console.log('Translate modul inintialized');
            },

            trans: function(translatable) {
                // console.log(TranslateFile);
                var arr = TranslateFile;
                var tmp;
                $.each(arr, function(index, obj) {
                    if(obj.label == translatable) {
                        $.each(obj.translations, function(index, objT) {
                            if(objT.lang == window.localStorage.lang) { 
                                tmp = objT.translate;
                            }
                        });
                    }
                    
                });

                if(tmp === undefined) tmp = translatable;

                return tmp;

            },

            setTranslations: function(DOMlist) {
                var self = this;
                $.each(DOMlist, function(index, DOM) {
                    $(DOM).text(self.trans($(DOM).text()));
                    // console.log($(DOM).text());
                });
            },

        });

});
