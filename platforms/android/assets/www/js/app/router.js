define(function (require) {

    "use strict";

    var $             = require('jquery'),
        Backbone      = require('backbone'),
        LoginView     = require('app/views/Login'),
        DashboardView = require('app/views/Dashboard'),
        ClientView    = require('app/views/Client'),
        DrivingView   = require('app/views/Driving'),
        CalendarView  = require('app/views/Calendar'),
        CameraView    = require('app/views/Camera'),
        SettingsView  = require('app/views/Settings'),
        Header        = require('app/views/Header'),
        Footer        = require('app/views/Footer'),
        DataTables    = require('jquery.dataTables'),
        JQueryUI      = require('jquery-ui'),
        JQueryUITouch = require('jquery-ui-touch-punch.min'),
        // Hammer        = require('hammer.min.js'),
        Moment        = require('moment'),
        FullCalendar  = require('fullcalendar.min'),
        Translate     = require('trans/translate');


    return Backbone.Router.extend({

        translate: new Translate(),
        
        initialize: function(options) {
            this.layout = options.layout;
          },

        routes: {
            ""            : "login",
            "login"       : "login",
            "home"        : "dashboard",
            "clients"     : "clientList",
            "drivings"    : "drivingList",
            "calendar"    : "calendar",
            "camera"      : "camera",
            "settings"    : "settings"
        },
        _init: function() {
           
            $.ajaxSetup({
                type: 'POST',
                data : {user : ((typeof(window.sessionStorage.teacher) !== "undefined") ? JSON.parse(window.sessionStorage.teacher) : "")},
                dataType: 'json'
            });
            clearInterval(window.tableRefresh);
        },

        login: function () {
            window.sessionStorage.clear();
            this._init();
            this.layout.setHeader(new Header());
            this.layout.setContent(new LoginView());
        },

        dashboard: function () {
            this._init();
            this.layout.setHeader(new Header());
            this.layout.setContent(new DashboardView());
            this.layout.setFooter(new Footer());
        },

        clientList: function (id) {
            this._init();
            this.layout.setHeader(new Header());
            this.layout.setContent(new ClientView());

            $("#Clients")
                .on('xhr.dt', function ( e, settings, json, xhr ) {
                    if(json.status == 'ERROR') {
                        $('#Clients tbody').empty();
                        $('#Clients tbody').append('<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">' + settings.oLanguage.infoEmpty + '</td></tr>');
                    }
                })
                .DataTable({
                    info: false,
                    paging: false,
                    serverSide: false,
                    dom: '<"toolbar">frtip',
                    ajax: {
                        url: "http://www.teooria.ee/api/v1/clients",
                        type: 'POST'
                    },
                    columns: [
                        { data: "clientName" },
                        { data: "courseName"},
                        { data: "email" },
                        { data: "phone" }
                    ],
                    createdRow: function( row, data, dataIndex ) {
                          $(row).addClass( 'client-item' ).data({'clientID': data.clientId, 'ledgerID': data.ledgerId, 'clientPhoto': data.image, 'clientEmail': data.email, 'clientPhone': data.phone, 'clientName': data.clientName});
                    },
                    responsive: true,
                    language: {
                        search: "_INPUT_",
                        searchPlaceholder: this.translate.trans('Otsi...'),
                        lengthMenu: this.translate.trans('Näita _MENU_ õpilast lehel'),
                        zeroRecords: this.translate.trans('Õpilased puuduvad'),
                        info: this.translate.trans('Näitan _PAGE_ - _PAGES_'),
                        infoEmpty: this.translate.trans('Õpilased puuduvad'),
                        infoFiltered: this.translate.trans('(filtreeritud _MAX_ õpilaste seast)'),
                        loadingRecords: this.translate.trans('Laen õpilasi...'),
                        paginate: {
                            first: this.translate.trans('Algus'),
                            previous: this.translate.trans('Eelmine'),
                            next: this.translate.trans('Järgmine'),
                            last: this.translate.trans('Viimane')
                        },
                        aria: {
                           sortAscending:  this.translate.trans(': sorteeri kasvavalt'),
                            sortDescending: this.translate.trans(': sorteeri kahanevalt')
                        }
                    },
                });
            $('div.toolbar').html('<span>' + this.translate.trans('ÕPILASED') + '</span>');
            
            this.layout.setFooter(new Footer());
        },

        drivingList: function () {
            this._init();
            this.layout.setHeader(new Header());
            this.layout.setContent(new DrivingView());

            $('#Drivings thead tr th').first().html('<input type="text" id="datepicker" style="min-width: 100px; margin-right: 10px; width: 80%;" onclick="stopPropagation(event);" placeholder="' + this.translate.trans("Vali kuupäev") + '" readonly="true"/>');
            $('#Drivings #datepicker').datepicker({
                dateFormat: 'yy-mm-dd',
                showButtonPanel: true,
                firstDay: 1,
                beforeShow: function(input, inst) {
                     setTimeout(function() {  
                       var buttonPane = $( input )  
                         .datepicker( "widget" )  
                         .find( ".ui-datepicker-buttonpane" );  
                      
                       var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Clear</button>');  
                       btn  
                        .unbind("click")  
                       .bind("click", function () {  
                        $.datepicker._clearDate( input );  
                      });  
                      
                       btn.appendTo( buttonPane );  
                      
                     }, 1 );  
                }
            });
            // $('#Drivings #datepicker').datepicker('setDate', new Date());
         
            $('#Drivings #datepicker').datepicker().on("input change", function (e) {
                $("#Drivings").DataTable().ajax.reload(null, false);
            });


            var drivingTable = $("#Drivings")
            .on('xhr.dt', function ( e, settings, json, xhr ) {
                if(json.status == 'ERROR') {
                    $('#Drivings tbody').empty();
                    $('#Drivings tbody').append('<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">' + settings.oLanguage.infoEmpty + '</td></tr>');
                }
            })
            .DataTable({
                info: false,
                paging: false,
                serverSide: false,
                deferRender: true,
                dom: '<"toolbar">frtip',
                ajax: {
                    url: "http://www.teooria.ee/api/v1/drivings",
                    type: 'POST',
                    data: function(d) {
                        d.dateFilter = $('#Drivings #datepicker').val();
                    }

                },
                columns: [
                    { data: "driving.date", type: "date"},
                    { data: "driving.time", type: "time" },
                    { data: "client.name" },
                    { data: "course.name" },
                    { data: "driving.info"},
                    { data: "driving.statusDisplay" }
                ],
                order : [[0, "asc"]],
                createdRow: function( row, data, dataIndex ) {
                    $(row).addClass( 'driving-item' ).data({'clientID': data.client.clientId, 'drivingID': data.driving.id, 'ledgerID': data.course.ledgerId, 'clientPhone': data.client.phone, 'clientEmail': data.client.email, 'status': data.driving.status, 'clientName': data.client.name, 'datetime': data.driving.formattedDate, 'timestamp': data.driving.rawDate, 'clientPhoto': data.client.image});
                },
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: this.translate.trans('Otsi...'),
                    lengthMenu: this.translate.trans('Näita _MENU_ sõitu lehel'),
                    zeroRecords: this.translate.trans('Sõidud puuduvad'),
                    info: this.translate.trans('Näitan _PAGE_ - _PAGES_'),
                    infoEmpty: this.translate.trans('Sõidud puuduvad'),
                    infoFiltered: this.translate.trans('(filtreeritud _MAX_ sõitude seast)'),
                    loadingRecords: this.translate.trans('Laen sõite...'),
                    paginate: {
                        first: this.translate.trans('Algus'),
                        previous: this.translate.trans('Eelmine'),
                        next: this.translate.trans('Järgmine'),
                        last: this.translate.trans('Viimane')
                    },
                    aria: {
                       sortAscending:  this.translate.trans(': sorteeri kasvavalt'),
                        sortDescending: this.translate.trans(': sorteeri kahanevalt')
                    }
                },
            });
            $('div.toolbar').html('<span>' + this.translate.trans('SÕIDUD') + '</span>');

            $("#Drivings thead input").on( 'keyup change', function () {
                drivingTable
                    .column( $(this).parent().index()+':visible' )
                    .search( this.value )
                    .draw();
            });
            window.tableRefresh = setInterval( function () {
                if(typeof(window.sessionStorage.teacher) === 'undefined') {
                    clearInterval(tableRefresh);
                } else {
                    drivingTable.ajax.reload(null, false);
                }
            }, 5000 );

            this.layout.setFooter(new Footer());
        },

        calendar: function() {
            this._init();
            this.layout.setHeader(new Header());
            this.layout.setContent(new CalendarView());
            this.layout.setFooter(new Footer());

            var self = this;

            // HELPER FUNC

            //Combobox
            $.widget( "custom.combobox", {
                _create: function() {
                    this.wrapper = $( "<span>" )
                      .addClass( "places-combobox" )
                      .insertAfter( this.element )
                      .append("<span class='error place-error-msg'>Koht jäi sisestamata.</span>"); // Added error box
         
                    this.element.hide();
                    this._createAutocomplete();
                    this._createShowAllButton();
                  },
         
                _createAutocomplete: function() {
                    var selected = this.element.children( ":selected" ),
                        value = selected.val() ? selected.text() : "";
             
                    this.input = $( "<input>" )
                        .appendTo( this.wrapper )
                        .val(value)
                        .attr('placeholder', self.translate.trans('Vali asukoht'))
                        .addClass( "places-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: $.proxy( this, "_source" )
                        })
                        .tooltip({
                            tooltipClass: "ui-state-highlight"
                        });
             
                    this._on( this.input, {
                        autocompleteselect: function( event, ui ) {
                            ui.item.option.selected = true;
                            this._trigger( "select", event, {
                              item: ui.item.option
                            });
                            this.element.trigger('change');
                            // console.log(this.element);
                        },
                          
                        autocompletechange: "_removeIfInvalid"
                    });

                },
                autocomplete: function(value) {
                    this.element.val(value);
                    this.input.val(value);
                },
         
                _createShowAllButton: function() {
                    var input = this.input,
                        wasOpen = false;
             
                    $( '<a>' )
                        .attr( 'tabIndex', -1 )
                        .attr( 'title', 'Näita kõiki' )
                        .tooltip()
                        .appendTo( this.wrapper )
                        .button({
                            icons: {
                                primary: 'ui-icon-triangle-1-s'
                            },
                            text: false
                        })
                        .removeClass( 'ui-corner-all' )
                        .addClass( 'places-combobox-toggle ui-corner-right' )
                        .mousedown(function() {
                            wasOpen = input.autocomplete( 'widget' ).is( ':visible' );
                        })
                        .click(function() {
                            input.focus();
             
                            // Close if already visible
                            if ( wasOpen ) {
                              return;
                            }
             
                            // Pass empty string as value to search for, displaying all results
                            input.autocomplete( 'search', '' );
                        });
                  },
         
                _source: function( request, response ) {
                    var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), 'i' );
                    response( this.element.children( 'option' ).map(function() {
                        var text = $( this ).text();
                        if ( this.value && ( !request.term || matcher.test(text) ) )
                            return {
                                label: text,
                                value: text,
                                option: this
                            };
                    })  );
                },
         
                _removeIfInvalid: function( event, ui ) {
                    // Selected an item, nothing to do
                    if ( ui.item ) {
                        return;
                    }
             
                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                        valueLowerCase = value.toLowerCase(),
                        valid = false;
                    this.element.children( 'option' ).each(function() {
                        if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                            this.selected = valid = true;
                            return false;
                        }
                    });
             
                    // Found a match, nothing to do
                    if ( valid ) {
                        return;
                    }
                },
         
                _destroy: function() {
                    this.wrapper.remove();
                    this.element.show();
                }
            });

            $( "#placeDropdown" ).combobox();

            //

            $('.category').val(window.localStorage.category);
            $('.gearbox').val(window.localStorage.gearbox);
            $('#placeDropdown').combobox('autocomplete', window.localStorage.place);
            
            

            $.ajax({
                url: 'http://www.teooria.ee/api/v1/drivings/getPlaces/',
                type: 'POST',
                data: {teacherId: (JSON.parse(window.sessionStorage.teacher)).teacherId },
                success: function(data) {
                    // console.log(data);
                    if(data.status == 'OK') {
                        $('#placeDropdown').append('<option value=""></option>');
                        $.each(data.data, function(index, place) {
                            $('#placeDropdown').append('<option value="' + place.place + '">' + place.place + '</option>');
                        });
                    } else {
                        $('#placeDropdown').append('<option value="">' + this.translate.trans('Asukohad puuduvad') + '</option>');
                    }
                },
            });

            $('#calendar').fullCalendar({
                // lang: 'ee',
                buttonText: {
                    today: this.translate.trans('täna'),
                    month: this.translate.trans('kuu'),
                    week: this.translate.trans('nädal'),
                    day: this.translate.trans('päev'),
                    agenda: this.translate.trans('Päevakord')
                },
                monthNames: this.translate.trans('jaanuar_veebruar_märts_aprill_mai_juuni_juuli_august_september_oktoober_november_detsember').split('_'),
                monthNamesShort: this.translate.trans('jaan_veebr_märts_apr_mai_juuni_juuli_aug_sept_okt_nov_dets').split('_'),
                dayNames: this.translate.trans('pühapäev_esmaspäev_teisipäev_kolmapäev_neljapäev_reede_laupäev').split('_'),
                dayNamesShort: this.translate.trans('P_E_T_K_N_R_L').split('_'),
                weekNumberTitle: this.translate.trans('Näd '),

                firstDay: 1,
                axisFormat: 'HH:mm',
                timeFormat: 'HH:mm',
                titleFormat: 'D. MMMM YYYY',
                columnFormat: {
                    month: 'ddd', 
                    week: 'ddd', 
                    day: 'dddd'
                },
                allDaySlot: false,
                header: {
                    left: 'prev,next,today',
                    center: 'title',
                    right: ',agendaWeek'
                },
                defaultView: 'agendaWeek', 
                backgroundColor: '#fff',
                weekNumbers: true,
                theme: false,
                selectable: true,
                editable: true,
                lazyFetching: false,
                allDayDefault: false,

                minTime: '07:00:00',
                maxTime: '22:00:00',
                timezone: 'local',
                defaultTimedEventDuration: JSON.parse(window.sessionStorage.teacher).duration,
                eventDurationEditable: false,
                displayEventEnd: false,
                height: 'auto', 
                slotDuration: '00:' + JSON.parse(window.sessionStorage.teacher).duration + ':00',
                
                eventRender: function(event, element, view) {
                    $(element).addTouch();
                    $(element).css({'margin-top': '1px', 'margin-bottom': '1px'})
                },

                eventAfterRender: function(event, element, view) {
                    $(element).removeClass('fc-short');
                },

                eventSources: [ 
                    {
                        events: function(start, end, timezone, callback) {
                            start = new Date(start);
                            end = new Date(end);
                            var startDate = start.getFullYear() + '-' + (start.getMonth() + 1) + '-' + start.getDate();
                            var endDate = end.getFullYear() + '-' + (end.getMonth() + 1) + '-' + end.getDate();
                            var category = $('.category').val();
                            // console.log(category);
                            var gearbox = $('.gearbox').val();
                            // console.log(gearbox);
                            var place = $('#placeDropdown').val();
                            $.ajax({
                                url: 'http://www.teooria.ee/api/v1/drivings/getEvents/' + (JSON.parse(window.sessionStorage.teacher)).teacherId,
                                type: 'POST',
                                data: {
                                    start: startDate,
                                    end: endDate,
                                    category: category,
                                    gearbox: gearbox,
                                    place: place
                                },
                                dataType: 'json',
                                success: function(data) {
                                    // console.log(data);
                                    var events = [];
                                    if(data.status == 'OK') {
                                        $.each(data.data, function(key, eventItem) {
                                            var className = 'scheduled';
                                            var filters = eventItem.filters;
                                            var editable = true;
                                            if(filters.gearbox && filters.category && filters.place) {
                                                className += ' current';
                                            } 
                                            if(eventItem.details.registered) {
                                                className = 'registered';
                                                editable = false;
                                            }
                                            if(eventItem.details.done) {
                                                className = 'done';
                                                editable = false;

                                            }
                                            events.push({
                                                id: eventItem.details.id,
                                                title: /*eventItem.details.place + */((eventItem.details.firstname != '') ? ' - ' + eventItem.details.firstname : ''),
                                                start: eventItem.details.start, // will be parsed'
                                                end: eventItem.details.end, // will be parsed
                                                className: className,
                                                editable: editable
                                            });
                                            
                                        });

                                        callback(events);
                                    } 
                                    
                                },
                                error: function() {
                                    alert('there was an error while fetching events!');
                                },
                            });
                        },
                        textColor: '#000'
                        
                    }, 
                   
                ],

                eventClick: function(event, element) {

                    

                    if(event.className.indexOf('current') >= 0) {
                        navigator.notification.confirm('Sõidutunni kustutamine', function(button) {
                            if(button == 1) {
                                $.ajax({
                            url: 'http://www.teooria.ee/api/v1/drivings/deleteDriving/' + event.id,
                            type: 'POST',
                            success: function(data) {
                                if(data.status == 'OK') {
                                    // console.log(data);
                                    $('#calendar').fullCalendar('removeEvents', event.id);
                                } else {
                                    // console.log(data.message);
                                }
                            },
                            error: function() {
                                console.log('Wrong url inserted. Use .../deleteDriving/1');
                            }
                        });
                            } else {
                                revertFunc();
                            } 
                        }, 'Soovite sõidutunni kustutada?', 'Kustuta, Tagasi');
                        
                    } 
                    
                },

                select: function(start, end, allDay) {

                    var category = $('.category').val();
                    // console.log(category);
                    var gearbox = $('.gearbox').val();
                    // console.log(gearbox);
                    var place = $('.places-combobox-input').val().trim();
                    // console.log(place);
                    var duration = (JSON.parse(window.sessionStorage.teacher)).duration; 
                    var startDate = new Date(start);
                    var timestamp = startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate() + ' ' +
                                    ((startDate.getHours() < 10) ? "0" + startDate.getHours() : startDate.getHours()) + 
                                    ":" + 
                                    ((startDate.getMinutes() < 10) ? "0" + startDate.getMinutes() : startDate.getMinutes());
                    var endDate = new Date(startDate);
                    endDate.setMinutes(endDate.getMinutes() + parseInt(duration));
                    end = endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate() + ' ' +
                                    ((endDate.getHours() < 10) ? "0" + endDate.getHours() : endDate.getHours()) + 
                                    ":" + 
                                    ((endDate.getMinutes() < 10) ? "0" + endDate.getMinutes() : endDate.getMinutes());
                    // console.log(start.format() + ' : ' + end);
                    if (place !== '') {
                        console.log(place);
                        $('.place-error-msg').hide();
                        $('#calendar').fullCalendar('renderEvent',
                            {
                                // title: place,
                                start: start,
                                end: endDate,
                                className: 'current'
                            },
                            false // make the event "stick"
                        );

                        $.ajax({
                            url: 'http://www.teooria.ee/api/v1/drivings/saveNewDriving',
                            data: { // TODO! REFRACTORING
                                teacherId: (JSON.parse(window.sessionStorage.teacher)).teacherId,
                                schoolId: (JSON.parse(window.sessionStorage.teacher)).schoolId,
                                place: place,
                                duration: duration,
                                category: category,
                                gearbox: gearbox,
                                timestamp: timestamp,
                                end: end
                            },
                            success: function(data) {
                                // console.log(data);
                                if(data.status == 'OK') {
                                    // console.log(data);
                                    $('#calendar').fullCalendar('unselect');
                                    $('#calendar').fullCalendar('refetchEvents');
                                } else if(data.status == 'ERROR') {
                                    navigator.notification.alert('Sõidu salvestamine ebaõnnestus. \nProovi hiljem uuesti', 
                                                                null, 
                                                                'Ebaõnnestus', 
                                                                'Sulge');
                                } else {
                                    navigator.notification.alert('Ilmnes tõrge. \nProovi hiljem uuesti', 
                                                                null, 
                                                                'Tõrge', 
                                                                'Sulge');
                                } 
                            }
                        });
                        
                    } else {
                         $('.places-combobox > input').focus();
                         $('.place-error-msg'). show();
                    }

                },

                eventDrop: function(event, delta, revertFunc) {
                    // console.log(event.start.format());
                    navigator.notification.confirm('Soovite muuta sõidutunni aega?', function(button) {
                        if(button == 1) {
                            $.ajax({
                                url: 'http://www.teooria.ee/api/v1/drivings/updateDriving/' + event.id,
                                type: 'POST',
                                data: {
                                    teacherId: (JSON.parse(window.sessionStorage.teacher)).teacherId,
                                    schoolId: (JSON.parse(window.sessionStorage.teacher)).schoolId,
                                    // place: (event.title.split(' (', 1))[0],
                                    timestamp: event.start.format()
                                },
                                success: function(data) {
                                    if(data.status == 'OK') {
                                        console.log(data);
                                    } else {
                                        console.log(data.message);
                                    }
                                },
                                error: function() {
                                    console.log('Wrong url inserted. Use .../updateDriving/1');
                                }
                            });
                        } else {
                            revertFunc();
                        } 
                    }, 'Sõidutunni muudatus!', 'Muudan, Tagasi');
                }
            });

        }, 
        camera: function() {
            this._init();
            this.layout.setHeader(new Header());
            this.layout.setContent(new CameraView());
            this.layout.setFooter(new Footer());

            $('#cameraPic').attr('src', window.image);
        },

        settings: function() {
            this._init();
            this.layout.setHeader(new Header());
            this.layout.setContent(new SettingsView());
            this.layout.setFooter(new Footer());

            // GET DRIVING LESSON DURATION SETTING 
        
            $.ajax({
                url: 'http://www.teooria.ee/api/v1/drivings/getDrivingDurationSetting/' + (JSON.parse(window.sessionStorage.teacher)).teacherId,
                type: 'POST',
                success: function(data) {
                    // console.log(data);
                    if(data.status == 'OK') {
                        $('.durations > input:radio[value="'+ data.data.duration +'"]').attr('checked', true);
                    }
                },
            });

            // GET LANGUAGE SETTING 
            $('.languages > input:radio[value="'+ window.localStorage.lang +'"]').attr('checked', true);
        }

    });

});