define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone');

        return Backbone.Model.extend({

            defaults: {
                ID: 1,
            },

            initialize: function (attributes, options) {
                // console.log('Teacher initialized');
            }

        });

});