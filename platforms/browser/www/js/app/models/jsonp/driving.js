define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),

        Driving = Backbone.Model.extend({

            urlRoot: "http://localhost:3000/drivings",

            initialize: function () {
                this.reports = new DrivingCollection();
                this.reports.url = this.urlRoot + "/" + this.id + "/reports";
            }

        }),

        DrivingCollection = Backbone.Collection.extend({

            model: Driving,

            url: "http://localhost:3000/drivings"

        }),

        originalSync = Backbone.sync;

    Backbone.sync = function (method, model, options) {
        if (method === "read") {
            options.dataType = "jsonp";
            return originalSync.apply(Backbone, arguments);
        }
    };

    return {
        Driving: Driving,
        DrivingCollection: DrivingCollection
    };

});