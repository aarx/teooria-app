define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/Camera.html'),
        Translate           = require('trans/translate'),

        template = _.template(tpl);

    return Backbone.View.extend({

        // startTime: 0,
        // endTime: 0,

        initialize: function () {
            // this.render();
        },

        render: function () {
            this.$el.html(template);

            var translate = new Translate();
            var labels = this.$('.translate');
            // console.log(labels);
            translate.setTranslations(labels);
            return this;
        },


        events : {
            'click #openCamera' : 'takePhoto',
            'click #confirmNewPic' : 'confirmNewPic'
        },

        takePhoto: function() {
            // this.startTime = (new Date()).getTime();
            console.log(window.clientId);
            // navigator.notification.alert(window.cameraOptions);
            navigator.camera.getPicture(this.onSuccess, this.onError, window.cameraOptions );
            // this.endTime = (new Date()).getTime();
            // $('.geolocation-log').append('<p class="succeess">[CAMERA] startTime=' 
            //     + this.startTime + ', endTime=' + this.endTime + ', endTime-startTime=' 
            //     + (this.endTime-this.startTime) + '</p>');

        },

        onSuccess: function(data) {
            $('#cameraPic').attr('src', 'data:image/jpeg;base64,' + data);
            $('#confirmNewPic').show();
        },
        onError: function(message) {
            console.log(message);
            navigator.notification.alert('Tekkis tõrge: ' + message);
        },

        confirmNewPic: function() {
            // $('#confirmNewPic').text('Laen...').attr('disabled', true);
            // console.log(window.clientId);
            $('.new-pic-error-msg').hide();
            $('.new-pic-saved-msg').hide();
            $.ajax({
                url: 'http://www.teooria.ee/api/v1/clients/addClientPhoto/' + window.clientId,
                type: 'POST',
                data: {image: $('#cameraPic').attr('src')},
                success: function(data) {
                    // navigator.notification.alert(data); 
                    if(data.status == 'OK') {
                        $('#confirmNewPic').hide();
                        $('.new-pic-saved-msg').show();
                    } else {
                        $('.new-pic-error-msg').show();
                    }
                },
                error: function() {
                    $('.new-pic-error-msg').show();
                }
            });
            // $('#confirmNewPic').text('Kinnita').removeAttr('disabled');
        }

    });

});