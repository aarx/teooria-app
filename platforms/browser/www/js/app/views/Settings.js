define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/Settings.html'),
        Translate           = require('trans/translate'),
        
        template = _.template(tpl);

    return Backbone.View.extend({

        translate: new Translate(),

        initialize: function () {
            // this.render();
        },

        render: function () {
            this.$el.html(template);

            var translate = new Translate();
            var labels = this.$('.translate');
            // console.log(labels);
            translate.setTranslations(labels);
            return this;
        },

        events: {
            'change .durations > input[name="duration"]' : 'setDuration',
            'change .languages > input[name="lang"]'     : 'setLang'
        },

        // SET DRIVING LESSON DURATION:
        setDuration: function(event) {
            var duration = $('.durations > input[name="duration"]:checked').val();
            $.ajax({
                url: 'http://www.teooria.ee/api/v1/drivings/setDrivingDurationSetting/' + (JSON.parse(window.sessionStorage.teacher)).teacherId,
                data: {duration: duration},
                success: function(data) {
                    if(data.status == 'OK') {
                        // console.log(data);
                        var teacherObj = JSON.parse(window.sessionStorage.teacher)
                        teacherObj.duration = duration;
                        window.sessionStorage.teacher = JSON.stringify(teacherObj);
                        // navigator.notification.confirm(
                        //     'Uue sõidutunni kestvuse seadistuse rakendumiseks peate uuesti sisse logima.',
                        //     null,
                        //     'Sõidutunni kestvuse muudatus',
                        //     'Olgu');
                    } else if(data.status == 'ERROR') {
                        navigator.notification.confirm(
                            'Seadistuse muutmine ebaõnnestus. \nProovi hiljem uuesti.',
                            null,
                            'Ebaõnnestus',
                            'sulge');
                    } else {
                        navigator.notification.confirm(
                            'Tekkis tõrge. \nProovi hiljem uuesti.',
                            null,
                            'Tõrge',
                            'sulge');
                    }
                }
            });
        },
        setLang: function(event) {
            var language = $('.languages > input[name="lang"]:checked').val();
            window.localStorage.lang = language;
        }

    });

});