define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/Client.html'),
        Translate           = require('trans/translate'),

        template = _.template(tpl);

    return Backbone.View.extend({

        initialize: function () {
            this.render();
        },

        render: function () {
            this.$el.html(template);

            var translate = new Translate();
            var labels = this.$('.translate');
            // console.log(labels);
            translate.setTranslations(labels);
            return this;
        },


        events : {
            'click .client-item'   : 'openClient',
        },

        resetActionBox : function() {
            $('.actions-box > .profile-setting').find('.activity:nth-child(1)').attr('id', 'Dummy1').text('VALIK1').removeClass('hidden');
            $('.actions-box > .profile-setting').find('.activity:nth-child(2)').attr('id', 'Dummy2').text('VALIK2').removeClass('hidden');
            $('.actions-box > input.drivingID').remove();
            $('#forward').removeClass('hidden');
        },

        openClient: function(event) {
            var dataObj = $(event.currentTarget).data();
            window.image = dataObj.clientPhoto;
            var email = dataObj.clientEmail;
            var phone = dataObj.clientPhone;
            var studentName = dataObj.clientName;
            window.clientId = dataObj.clientID;
            

            this.resetActionBox();
            $('.actions-box > .profile-setting').find('#Dummy1').attr('id', 'RegisterNewDriving').text('REGISTREERI UUS SÕIT');
            $('.actions-box > .profile-setting').find('#Dummy2').addClass('hidden');
            $('.actions-box > .profile-setting').find('.cancel').addClass('hidden');
            $('.actions-box > .profile-setting > .image > a > img').attr('src', window.image);


            $('.actions-box > input.clientID').val(dataObj.clientID); // TODO! Remove in later versions
            $('.actions-box > input.ledgerID').val(dataObj.ledgerID);

            $('.actions-box > .profile-setting > .actions-box-header').html('<h1>' + studentName + '</h1><div class="btn-container"><p class="email"><a href="mailto:' + email + '"><i class="fa fa-envelope"></i></a></p><p class="phone"><a href="tel:' + phone + '"><i class="fa fa-phone-square"></i></a></p><p class="phone"><a href="sms:' + phone + '"><i class="fa fa-commenting"></i></a></p></div>');


            $('.actions-box').slideDown();
        },
        
    });

});