define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        SignaturePad        = require('signature_pad'),
        Header              = require('app/views/Header'),
        Footer              = require('app/views/Footer');

    return Backbone.View.extend({

      el: ".container",

      render: function() {
        return this;
      },

      setHeader: function(headerView) {
        if(this.header) {
          this.header.remove();
        }
        this.$("header").html((this.header = headerView).render().el);
      },

      setContent: function(contentView) {
        if(this.content) {
          this.content.remove();
        }
        if(this.footer) {
          this.footer.remove();
        }
          
        this.$(".content-area").html((this.content = contentView).render().el);
        var currentUrl = window.location.hash;
        var schoolId = typeof window.sessionStorage.teacher !== 'undefined' ? (JSON.parse(window.sessionStorage.teacher)).schoolId : '';
        if(/*currentUrl != '#home' && */currentUrl != '#login' && currentUrl != '' && schoolId == '155') {
          this.$(".content-area").append('<div>'
                                        +   '<button class="clear-log" onclick=$(".geolocation-log").empty();>Puhasta logi</button>'
                                        +   '<div class="geolocation-log" style="margin-bottom:30px;height:200px; width:100%; overflow-y:scroll;background-color:#fff; color:#000"></div> '
                                        + '</div>');
        }
      },

      setFooter: function(footerView) {
        if(this.footer) {
          this.footer.remove();
        }
        this.$("footer").html((this.footer = footerView).render().el);
        
        var canvas = document.querySelector('canvas');
        document.signaturePad = new SignaturePad(canvas);
      }
    });

});